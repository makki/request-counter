include .env
export

.PHONY: run
run:
	go run main.go -port=$(PORT) -file=$(DATA_KEEPER_FILE)