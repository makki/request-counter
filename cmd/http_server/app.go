package app

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"task.simplesurance.com/config"
	"task.simplesurance.com/internal/delivery"
	"task.simplesurance.com/internal/repository"
	"task.simplesurance.com/internal/usecase"
	httputil "task.simplesurance.com/pkg/http"
)

func Start() {
	initConfig()
	repo := repository.GetFilePersister(config.C.DataKeeperFile)
	uc := usecase.NewCounter(repo)

	err := uc.LoadPersistentData()
	if err != nil {
		log.Printf("there is a problem loading data from repository: %s", err.Error())
	}
	maxRequestAge, pruneInterval := 1*time.Minute, 1*time.Second
	uc.PruneOldRequestsPeriodically(maxRequestAge, pruneInterval)

	handler := delivery.NewHttpHandler(uc)
	serveHttp(handler, config.C.Port)
}

func initConfig() {
	defaultCfg := &config.App{
		Port:           8080,
		DataKeeperFile: "request_data.gob",
	}
	config.Init(defaultCfg)
}

func serveHttp(handler delivery.Handler, port int) {
	registerRoutes(handler)
	fmt.Printf("Server is running at :%d\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		log.Println("the http server returned an error")
	}
}

func registerRoutes(handler delivery.Handler) {
	http.HandleFunc("/count", httputil.PanicRecoveryMiddleware(handler.HandleRequestCounter))
}
