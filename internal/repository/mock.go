package repository

import "task.simplesurance.com/internal/models"

type Mock struct {
	DataSavingError  error
	Data             []models.Request
	DataLoadingError error
}

var _ Counter = &Mock{}

func NewMock() *Mock {
	return &Mock{}
}

func (m *Mock) LoadData() ([]models.Request, error) {
	return m.Data, m.DataLoadingError
}

func (m *Mock) SaveData([]models.Request) error {
	return m.DataSavingError
}
