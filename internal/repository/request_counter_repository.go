package repository

import (
	"encoding/gob"
	"os"

	"task.simplesurance.com/internal/models"
)

type Counter interface {
	LoadData() ([]models.Request, error)
	SaveData([]models.Request) error
}

type filePersister struct {
	filePath string
}

var _ Counter = &filePersister{}

var instance *filePersister

func GetFilePersister(filePath string) Counter {
	if instance == nil {
		instance = &filePersister{
			filePath: filePath,
		}
	}
	return instance
}

func (f *filePersister) LoadData() ([]models.Request, error) {
	result := new([]models.Request)
	file, err := os.Open(f.filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	decoder := gob.NewDecoder(file)
	err = decoder.Decode(result)
	if err != nil {
		return nil, err
	}
	return *result, nil
}

func (f *filePersister) SaveData(data []models.Request) error {
	file, err := os.Create(f.filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := gob.NewEncoder(file)
	return encoder.Encode(data)
}
