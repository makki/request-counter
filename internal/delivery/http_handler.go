package delivery

import (
	"fmt"
	"log"
	"net/http"

	"task.simplesurance.com/internal/usecase"
)

type Handler interface {
	HandleRequestCounter(http.ResponseWriter, *http.Request)
}

type httpHandler struct {
	uc usecase.Counter
}

var _ Handler = httpHandler{}

func NewHttpHandler(uc usecase.Counter) httpHandler {
	return httpHandler{
		uc: uc,
	}
}

func (h httpHandler) HandleRequestCounter(w http.ResponseWriter, r *http.Request) {
	err := h.uc.IncrementRequests()
	if err != nil {
		log.Printf("unexpected error: %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal server error: unexpected response")
	}
	count := h.uc.CountRequests()
	fmt.Fprintf(w, "Total Requests in the Last 60 Seconds: %d", count)
}
