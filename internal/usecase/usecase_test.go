package usecase

import (
	"testing"
	"time"

	"task.simplesurance.com/internal/repository"
)

func TestPruneOldRequests(t *testing.T) {
	maxRequestAge := 10 * time.Second
	repo := repository.NewMock()
	uc := NewCounter(repo)
	for i := 0; i < 100; i++ {
		err := uc.IncrementRequests()
		if err != nil {
			t.Error(err)
		}
	}
	t.Logf("sleeping for %v", maxRequestAge)
	time.Sleep(maxRequestAge)
	uc.pruneOldRequests(maxRequestAge)
	count := uc.CountRequests()
	if count != 0 {
		t.Error("pruning old requests failed")
		return
	}
}

func TestIncrementRequest(t *testing.T) {
	repo := repository.NewMock()
	uc := NewCounter(repo)
	err := uc.IncrementRequests()
	if err != nil {
		t.Error(err)
	}
	c := uc.CountRequests()
	if c != 1 {
		t.Errorf("Incrementing requests failed.")
	}
}
