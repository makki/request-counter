package usecase

import (
	"sort"
	"sync"
	"time"

	"task.simplesurance.com/internal/models"
	"task.simplesurance.com/internal/repository"
)

type Counter interface {
	LoadPersistentData() error
	CountRequests() int
	IncrementRequests() error
	PruneOldRequestsPeriodically(maxRequestAge, pruneInterval time.Duration)
}

type requestCounter struct {
	repo     repository.Counter
	requests []models.Request
	sync.RWMutex
}

func NewCounter(r repository.Counter) *requestCounter {
	return &requestCounter{
		repo: r,
	}
}

var _ Counter = &requestCounter{}

func (r *requestCounter) LoadPersistentData() error {
	r.Lock()
	defer r.Unlock()

	var err error
	r.requests, err = r.repo.LoadData()
	return err
}

func (r *requestCounter) CountRequests() int {
	r.RLock()
	defer r.RUnlock()
	return len(r.requests)
}

func (r *requestCounter) IncrementRequests() error {
	r.Lock()
	defer r.Unlock()

	currentRequest := models.Request{
		T: time.Now(),
	}
	r.requests = append(r.requests, currentRequest)

	return r.repo.SaveData(r.requests)
}

func (r *requestCounter) PruneOldRequestsPeriodically(maxRequestAge, pruneInterval time.Duration) {
	ticker := time.NewTicker(pruneInterval)

	go func() {
		for range ticker.C {
			r.pruneOldRequests(maxRequestAge)
		}
	}()
}

func (r *requestCounter) pruneOldRequests(maxRequestAge time.Duration) {
	r.Lock()
	defer r.Unlock()

	idx := sort.Search(len(r.requests), func(i int) bool {
		return time.Since(r.requests[i].T) < maxRequestAge
	})
	if idx > 0 && idx <= len(r.requests) {
		r.requests = r.requests[idx:]
	}
}
