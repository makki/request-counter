
# Request Counter

This is a Go HTTP server that responds with a counter of the total number of requests it has received during the previous 60 seconds (moving window). The server persists data to a file so that it continues to return the correct numbers after being restarted.

## How to Run

- With the makefile: `make run`

- With Docker: `docker compose up`

## Service Endpoint
  - http://{HOST_IP}:{SERVICE_PORT}/count

## Prerequisites

Before running the server, make sure to copy the `env.example` file to `.env` and set the following environment variables:

- `HTTP_SERVER_PORT`: The port that the server should listen on (default: 8080)

- `REQUEST_DATA_FILE`: The name of the file to which the server should persist data (default: `request_data.gob`)

