# Build Stage
ARG GO_VERSION=1.21
ARG ALPINE_VERSION=3.18
FROM golang:$GO_VERSION-alpine$ALPINE_VERSION as build

WORKDIR /app

COPY . .

RUN go build -o myapp

#Run Stage
FROM alpine:3.18

WORKDIR /app

COPY --from=build /app/myapp /bin/myapp

EXPOSE 80

ARG FILE
ENV FILE=${FILE}

CMD myapp -port=80 -file=$FILE
