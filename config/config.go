package config

import "flag"

type App struct {
	Port           int
	DataKeeperFile string
}

var C App

func Init(defaultValues *App) {
	defaultApp := App{
		Port:           defaultValues.Port,
		DataKeeperFile: defaultValues.DataKeeperFile,
	}
	flag.IntVar(&C.Port, "port", defaultApp.Port, "HTTP server port")
	flag.StringVar(&C.DataKeeperFile, "file", defaultApp.DataKeeperFile, "Data Keeper filePath")
	flag.Parse()
}
